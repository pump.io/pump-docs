Frequently Asked Questions for sysadmins
========================================

Some common (and uncommon) issues that can come up while deploying
Pump, and some solutions to match.


----

I set `urlPort`, but my browser is still sending me to the wrong port.
----------------------------------------------------------------------

Clear your cache and try again.

What are the default credentials?
---------------------------------

There aren't any. You should register through the site normally after you've set it up.

If you want to be the only one on your system, just set ``disableRegistration`` in the configuration after you're done.

Is there an admin console?
--------------------------

Nope. Administration is done via the commandline.

Stuff in my brand-new account randomly doesn't work.
----------------------------------------------------

I get Mixed Content Blocker warnings in the browser console.
------------------------------------------------------------

Some actions randomly send requests to the wrong port in the devtools network pane.
-----------------------------------------------------------------------------------

This is usually caused by a reverse-proxy setup that was botched at some point. The solution is to make sure your URL is exactly the way you want it, including protocol scheme (i.e. HTTP or HTTPS - hopefully the latter!) and port. Then you need to drop your database (how to do this will depend on your Databank driver) and recreate any accounts you may have had on the server.

Note that if you did anything beyond just creating a new account you should choose a new URL that you haven't used yet, if possible.

(The reason that this issue happens is that ActivityStreams objects, which Pump heavily uses in its protocol, contain an id which Pump usually sets to the object's URL. A lot of these objects get created when new accounts are opened, and if this happens while the URL is somehow incorrect, the incorrect URL gets permanently embedded in the ActivityStreams object and doesn't update when the administrator adjusts the site configuration to fix the problem. This causes a myriad of problems which exhibit themselves in strange ways.)

My question isn't covered, or the solution didn't work for me.
--------------------------------------------------------------

Get in touch with the `community <https://github.com/e14n/pump.io/wiki/Community>`_. They'll be happy to help you out.
