Using the upstream pump.io systemd unit
==============================================

pump.io ships a systemd unit file which all administrators are
strongly encouraged to use.

This has several benefits:

1. You don't have to write a systemd unit yourself
2. Your node will automatically use production best practices, such as
   setting ``NODE_ENV`` to ``production`` in the environment
3. You automatically get an improved setup when we change the service
   file to e.g. take advantage of systemd security features

To start using the systemd unit shipped with pump.io, install a
symlink in systemd's unit file directory:

.. WARNING:: You should double-check that these paths are the correct
             paths for your system before running this command. In
             particular, some npm setups may install pump.io to
             ``/usr/local/lib/node_modules`` instead of
             ``/usr/lib/node_modules``.

::

   $ sudo ln -s /usr/lib/node_modules/pump.io/pump.io@.service /lib/systemd/system/

Now that you've got your unit installed, you need to figure out the
name of your unit. The pump.io systemd unit is parameterized so that
you can specify the systemd service name of your Databank driver. The
unit will arrange to have pump.io depend on the driver service being
available. (See the "Specifiers" section of `systemd.unit(8)
<https://www.freedesktop.org/software/systemd/man/systemd.unit.html>`_
for more details on how this works.)

For example, if your Databank driver is MongoDB, your pump.io unit
name would be ``pump.io@mongodb.service``. This will cause
``mongodb.service`` to be started before pump.io.

.. NOTE:: You can override individual service file options using
	  `drop-in files
	  <https://wiki.archlinux.org/index.php/Systemd#Drop-in_files>`_.

To actually `enable` the unit, try ``systemctl``:

::

   $ sudo systemctl enable pump.io@mongodb

If that command works, great! You're done. However, if you received
``Failed to execute operation: No such file or directory`` or
something similar, you are likely affected by `systemd bug #3010
<https://github.com/systemd/systemd/issues/3010>`_, so you'll need to
manually enable the service. This is easy enough with ``ln``:

::

   $ sudo ln -s /lib/systemd/system/pump.io@.service /etc/systemd/system/multi-user.target.wants/pump.io@mongodb.service

pump.io will now automatically start at boot. To start it immediately:

::

   $ sudo systemctl daemon-reload
   $ sudo systemctl start pump.io@mongodb

Congratulations! You're now using the systemd unit file shipped with pump.io.
