Configuration via CLI flags
===========================

The ``pump`` daemon accepts configuration values via options passed
directly on the commandline. Flags start with ``--`` - for example,
the ``port`` key can be set by passing ``--port``. Camelcasing like
``urlPort`` should be replaced with ``-`` (i.e.  ``urlPort`` becomes
``--url-port``). Keys whose value is an object can be specified using
``.`` to separate nested keys. For example, the ``params`` key set to
the following object:

::

    { "host": "localhost" }

can be set by passing ``--params.host localhost``.

CLI flags always take precedence over everything else, including
environment variables and JSON configuration files.

For a list of all available configuration values, see `the reference
list <reference.html>`_.
