About Docker images
===================

The pump.io project provides official support for Docker through
images published on `Docker Hub
<https://hub.docker.com/r/pumpio/pump.io/>`_. Docker images are built
automatically by Travis CI and will be available within 24 hours of an
npm release.

Consider using the Docker images if you have existing Docker
infrastructure or experience. For installs where the administrator
doesn't already have a preference for Docker, we recommend npm-based
installation.

This document assumes the reader is already familiar with Docker.

Installation
------------

The Docker images are published to ``pumpio/pump.io`` on `Docker Hub
<https://hub.docker.com/r/pumpio/pump.io/>`_. You can pull the latest
stable, for example, with:

::

   $ docker pull pumpio/pump.io:stable

All `release channels
<https://github.com/pump-io/pump.io/wiki/Release-cycle>`_ are
available on Docker Hub. Each image is tagged with its exact version
identifier, and the latest stable release, beta release, and nightly
``master`` builds are tagged with ``stable``, ``beta``, and ``alpha``
respectively. Each individual release is also tagged appropriately;
the Docker tag names are the exact same as the version strings found
in npm releases.

Note that if you choose to run alphas, the `same considerations
<https://github.com/pump-io/pump.io/wiki/Running-from-git-master>`_
apply as with npm-based installs.

Configuration
-------------

The Docker images are designed to be configured `via environment
variables <../configuration/via-environment-variables.html>`_. Note
that there are a couple differences in Docker images' configuration,
some of which change the defaults listed in the `configuration value
reference <../configuration/reference.html>`_.

Differences from npm builds
---------------------------

pump.io Docker images differ from npm-based installations in the
following ways:

* The daemon listens to port 80 by default
* ``datadir`` is set to ``/var/local/pump.io`` by default
* Logs are sent to stdout by default, not stderr
* Databank drivers for MongoDB, LevelDB, Redis, memcached, and
  ``databank-disk`` are already included
* The image is configured to run ``pump`` as an unprivileged
  ``pumpio`` user in the container

Note that ``/var/local/pump.io`` is configured to be a Docker volume,
which is why ``datadir`` is set there by default. The default for
``enableUploads`` (``false``) has not changed, however, so the admin
is free to leave uploads disabled.

.. WARNING:: In the future we may introduce important features that
             are automatically enabled when the ``datadir`` is
             set. These features will not be marked semver-major, so
             if you want to run the Docker image without the
             ``datadir`` volume mounted, you should explicitly set
             ``datadir`` to the empty string in the environment.

Security support
----------------

Docker images have the same security lifecycle as regular pump.io releases; you can find this policy on `the project wiki <https://github.com/pump-io/pump.io/wiki/Security>`_.

When using a regular npm-based install you need to make sure that
you're running a secure version of pump.io. When using Docker images,
you `also` need to make sure the image's native dependencies are fully
patched. To help with this, the pump.io project automatically
publishes new images within 24 hours of package upgrades becoming
available. For this reason, you are encouraged to regularly pull newer
pump.io images from Docker Hub. You can use the version number tags to
automate this so that you don't unexpectedly get new pump.io versions
(which may have breaking changes).

.. WARNING:: When security issues in pump.io itself are fixed, the
             version number increments which results in Docker images
             being published under a different tag - meaning that once
             a patch release is available, the 24-hour automated
             builds switch to that patch release.

	     Consequently, it's still important to keep track of
	     pump.io security releases even if you have automation
	     keeping your Docker images up-to-date.

As an example, say you're running pump.io 5.2.0 Docker images, and you
have cron configured to automatically pull newer 5.2.0 images from
Docker Hub. A (fictional) security issue arises in a native
dependency. In that case, you'll be fine - a newer image will be
published within 24 hours, and your cronjob will automatically pick
that up.

But let's say the security issue required a fix in the pump.io
codebase, not a native dependency. In that situation, the project
would release 5.2.1 as a security release and you would need to
upgrade your Docker image. If you didn't, not only would you not have
the pump.io fix, any future native dependency vulnerability fixes
wouldn't be pulled in by your cronjob. Upgrading to 5.2.1 images would
resolve both of these problems.

.. NOTE:: We many not turn off the infrastructure maintaining old
          release series immediately. If our `security policy
          <https://github.com/pump-io/pump.io/wiki/Security>`_ says a
          release is unsupported, you should consider it unmaintained
          even if new Docker images are still being published to the
          relevant tag.

If you have questions about this information, or if something is
confusing, please don't hesitate to contact the `community
<https://github.com/pump-io/pump.io/wiki/Community>`_.
