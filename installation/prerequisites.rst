Prerequisites
=============

You'll need four things to get started:

* node.js 6.x or higher
* npm 2.15.x or higher (4.x or above preferred)
* A database server (see below)
* The ``graphicsmagick`` package with the ``gm`` command

.. NOTE:: If you're planning to use npm to install the unpublished git
          master version of pump.io, npm 4 or better is a requirement,
          not a recommendation.

Database server
---------------

pump.io uses `databank <https://github.com/evanp/databank>`_ package
to abstract out the data storage for the system. Any databank driver
should work. Couchbase, MongoDB and Redis are probably the best bets
for production servers. ``disk`` and ``memory`` should only be used
for testing.

If you're confused, just use the MongoDB one, ``databank-mongodb``.

You can find other drivers like so:

::

    $ npm search databank

See the installation instructions for how to install a databank driver.

Note that you also need to install and configure your database server.

Changelog
---------

+-----------------+-----------------+----------------------------------------------------------+
| pump.io version | Change type     | Change                                                   |
+=================+=================+==========================================================+
| 0.3.0           | New dependency  | GraphicksMagick with the ``gm`` command is now required  |
+-----------------+-----------------+----------------------------------------------------------+
| 5.0.0           | Version change  | Node.js 4.x or better is now required                    |
+-----------------+-----------------+----------------------------------------------------------+
| 6.0.0           | Version change  | Node.js 6.x or better is now required                    |
+-----------------+-----------------+----------------------------------------------------------+
