Generic source-based install
============================

.. WARNING:: The pump.io project `strongly` recommends against
             source-based installations. They're harder to upgrade and
             the chances of the administrator making a mistake during
             installation is far higher.

             Unless you have a very good reason for doing this (for
             example, you need to patch the source code), you should
             do an `npm-based install <generic-npm.html>`_ instead.

             Feel free to ask the `community
             <https://github.com/pump-io/pump.io/wiki/Community>`_ if
             you're not sure about your particular situation.

First, install the `prereqisites <prerequisites.html>`_. Package names will
vary across distributions. In particular, how to install npm may vary
significantly, as some distributions ship severely outdated versions
of npm (or don't ship npm packages at all). Feel free to ask the
`community <https://github.com/pump-io/pump.io/wiki/Community>`_ for
help.

Once you have the prerequisites set up, you should clone the git
repository:

::

    git clone https://github.com/pump-io/pump.io.git

You can then install the dependencies using ``npm``:

::

    cd pump.io
    npm install
    npm run build

If you want to test the install, you can run:

::

    npm test

These tests are run on every commit to the codebase so they won't
catch actual bugs, but they may catch simple installation
problems. You can run them if you want, but they take a while so this
step is super optional.

You can now proceed to configuring pump.io. The recommended way for an
source-based install is to use `JSON configuration files
<../configuration/via-json-config-files.html>`_. You will also almost
certainly want to set up HTTPS at this point, perhaps using `Certbot
and Let's Encrypt <../configuration/certbot.html>`_.

.. WARNING:: If you're connecting your pump.io site with other
             software (such as federated servers or using Web
             clients), please note that most of them save OAuth keys
             based on your hostname and listening port. The following
             changes may make your relationships stop working.

             * Change of hostname
             * Change of port (from 8000 to 80 or even from HTTP to
               HTTPS)
             * Clearing your database or clearing some tables
             * Changing user nicknames

             We realize that these kind of changes are normal when
             someone's experimenting with new software, and there are
             (early, tentative) plans to make the software more robust
             in the face of this kind of change without sacrificing
             security, but for now it's a good idea to decide on your
             "real" domain name first before making connections to
             other sites.
