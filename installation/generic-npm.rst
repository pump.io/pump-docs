Generic npm-based install
=========================

.. NOTE:: This is the recommended install method.

First, install the `prereqisites <prerequisites>`_. Package names will
vary across distributions. In particular, how to install npm may vary
significantly, as some distributions ship severely outdated versions
of npm (or don't ship npm packages at all). Feel free to ask the
`community <https://github.com/pump-io/pump.io/wiki/Community>`_ for
help.

Once you have the prerequisites set up, you can install the npm package like so:

::

    npm install -g pump.io

Depending on your configuration, you may need to prefix this command
with ``sudo``. At this point all the files and dependencies should be
set up for you.

You can now proceed to configuring pump.io. The recommended way for an
npm-based install is to use `JSON configuration files
<../configuration/via-json-config-files.html>`_. You will also almost
certainly want to set up HTTPS at this point, perhaps using `Certbot
and Let's Encrypt <../configuration/certbot.html>`_.

.. WARNING:: If you're connecting your pump.io site with other
             software (such as federated servers or using Web
             clients), please note that most of them save OAuth keys
             based on your hostname and listening port. The following
             changes may make your relationships stop working.

             * Change of hostname
             * Change of port (from 8000 to 80 or even from HTTP to
               HTTPS)
             * Clearing your database or clearing some tables
             * Changing user nicknames

             We realize that these kind of changes are normal when
             someone's experimenting with new software, and there are
             (early, tentative) plans to make the software more robust
             in the face of this kind of change without sacrificing
             security, but for now it's a good idea to decide on your
             "real" domain name first before making connections to
             other sites.
