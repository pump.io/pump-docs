Upgrading to pump.io 4.x from 3.x
==================================

These are the instructions for upgrading pump.io 3.x to 4.x. They will
work for any release in the 3.x series and can be used to upgrade to
any release in the 4.x series, including beta releases.

.. NOTE:: If you're on pump.io 2.x or below, it's recommended to
          upgrade to pump.io 3.x before attempting to upgrade to
          4.x. It's not strictly necessary, but it makes things easier
          if things go wrong since there's less changes that could be
          causing problems.

If at any point you run into trouble, contact the `community
<https://github.com/e14n/pump.io/wiki/Community>`_ and they'll be
happy to sort you out.

For npm-based installs
----------------------

First, check if you have any plugins configured. If you haven't
defined a ``plugins`` key in your ``pump.io.json``, then you don't
have any plugins installed. If you `do` have plugins installed, see
"Migrating plugins" below.

To run the upgrade, invoke:

::

   $ sudo npm install -g pump.io@4

.. NOTE:: If you're trying to upgrade to the latest beta, specify
          ``pump.io@beta`` instead.

Complete your upgrade by restarting your pump.io process.

Note that the logic that loads the configuration was rewritten to be
more powerful. While it's expected that pump.io will behave the same,
there could be edge cases impacting your setup. Therefore, please
double-check that all your configuration values are still being
respected.

For source-based installs
-------------------------

First, check if you have any plugins configured. If you haven't
defined a ``plugins`` key in your ``pump.io.json``, then you don't
have any plugins installed. If you `do` have plugins installed, see
"Migrating plugins" below.

Next, fetch new changes, prefixing ``sudo`` as necessary:

::

   $ cd /path/to/your/installation/directory
   $ git fetch

If you've modified templates, you need to save the changes you've
made. You can check to see whether you've made changes by running
``git status`` and ``git log`` - if you have untracked changes
reported by ``git status``, save them with ``git stash``; if you have
committed changes reported by ``git log``, make a note of the commit
ids.

Now that we're prepared, we'll perform the upgrade itself by checking
out the relevant tag. You'll have to check what the latest version in
the 4.x series is using ``git tag`` - in this case, we're assuming
that 4.0.0 is the latest available.

::

   $ git checkout v4.0.0
   $ npm install

At this point, if you previously ran ``git stash``, you should run
``git stash apply``. If you made a note of commits, you should tell
Git to apply those commits on the new checkout using ``git
cherry-pick``.

If you just restored template changes, it's recommended that you run
the linting process to ensure that your changes are high-quality and
consistent with surrounding code:

::

   $ npm run lint:jade

Finally, regardless of whether or not you modified templates, you need
to rebuild client-side template resources which aren't checked into
git:

::

   $ npm run build

Complete your upgrade by restarting your pump.io process.

Note that the logic that loads the configuration was rewritten to be
more powerful. While it's expected that pump.io will behave the same,
there could be edge cases impacting your setup. Therefore, please
double-check that all your configuration values are still being
respected.

Migrating plugins
-----------------

pump.io 4.x upgrades the Express web framework to the 4.x series,
which contains breaking changes. This affects you because plugins
essentially get access to internal application details, including the
Express application object. This means that any plugin that relies on
the behavior of the app object is likely to be affected by Express
4.x's breaking changes. See the `Express 4.x change log
<https://github.com/expressjs/express/blob/master/History.md#400--2014-04-09>`_
for details.

Fixing this can be done in any number of ways - which way you choose
will depend on the nature and complexity of the plugin. For simpler
plugins, it may make sense to simply examine the plugin and compare
all uses of ``app`` with the Express change log. Alternately, it might
be easier to simply start the app, try out behaviors that your plugin
affects or creates, and see what crashes.
